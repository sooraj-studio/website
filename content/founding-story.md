---
title: Founding Story
description: Learn more about the inspiring story behind Grey Software!
---

![](https://lh4.googleusercontent.com/HbRaZkamshjd5fdpv4JHKJZRDi65hbnk4ofaZSbrCJMkoEiSThZ_JyjDxlBJ7t705S86XB8A5FeNcTd7TOeheX0ULlWmWv05bnlyHQ36eL7WGsfiH08rzUbIT4xQA3gf4K8skBgb=s0)

Ever since I can remember, adults in any given room would discuss the issues plaguing humanity with hopeless and
defeated commentary. This commentary would often end with a message that nobody could really disagree with:

### Things won't change until we have better education for everybody!

Better education, better health care, better infrastructure, we needed better everything.

But very few people have the skills, resources, or stamina needed to build a better world while the current world is
hostile and unforgiving.

### In the world I grew up in, getting out was the golden ticket, not staying and fixing things.

![](https://lh3.googleusercontent.com/orzqHX8BzPkfFWc1HLa0y5boNUV6x6a2pVOzC7GR5GGEOjTMBgSLkfckyC1GrlTwHOXdcVtpOqqZLDap2oXiSelByjlNjNlyBXUYmyqC8GmBTofetDwpIVSzWcX3eX7zOkxhOZV8=s0)

From my family members wanting to escape the realities of Pakistani life to the people at the frontier of science and
technology trying to escape the human condition on Earth, the message I grew up with was to survive while you can and
escape if you can!

My preferred escape was into the world of video games, entertainment, and information. By the end of high school in
Pakistan, I believed I was incompetent because of my two C and two D grade results in the A-Level exams. The systems
that governed my life had devastated my self-esteem and amplified my concern for the future.

That was when my family's golden ticket came in. We were accepted into Canada as permanent residents.

![](https://lh3.googleusercontent.com/M9KI9eo_rveiPkHhk4VC6Nw_18J5QGbbv_Ed93jH-VgIG9CPvpaKlkH_45ggPovhNZL1_5bplDVDJ33Q53JlyARUlbuyQ_5t0RLB5wuazLu_j5BkjhATifbK-y58cMMEEF2rbOpM=s0)

In Canada, I experienced a society with evolved laws, advanced technology, and a higher standard of living. It was
somewhere I had the safety to travel and the freedom to earn a dignified living as a high-schooler.

### Here, I repeated 12th grade in a healthier environment which allowed me to awaken the curious learner within me that had been put to sleep by factory schooling.

After doing well in high school, I was accepted into UofT Mississauaga's Computer Science program, where I explored my
passion for software and technology.

At this time, my vision didn't extend beyond getting a decent salary as a video game engineer working from wherever I
wanted in the world. However, since I found myself in such an exciting technological age, I couldn't help but dream
bigger.

People my age worldwide were starting impactful businesses and working for the world’s most advanced technology
companies. A close friend of mine got an internship at Microsoft, which made getting an internship at Google a more
viable reality.

Why did I want to work for Google?

![](https://lh5.googleusercontent.com/h1qyZD9HhZ8qZWYvY00Sq1xaS3CUl011U1MMWlwLjYEkrdaOSnY2Q1gruCzx-37wR6DoBMOCgw5qOoGg8m7yptHgk1wIjxOwb_2QNKHKWvUgEgntahKJA6wonFMYR3IxJYfEDJ3V=s0)

Well, the work Google was doing with the web, Android OS, and the Next Billion Users initiative was leveling the playing
field for people worldwide. I imagined myself collaborating on complex problems to help my people lead more peaceful and
prosperous lives.

During my second year, my application to Google didn't make it past their resume screening around, so I vowed to apply
the following year with more substantial engineering experience.

The summer before I applied again, I challenged myself to create and publish a math practice app. Designing and
engineering the app was one of the most transformative ventures of my life because it allowed me to appreciate the
complexity behind a successful app and develop the persistence needed to create it.

The app never really took off past about 300 users, but it was enough to get me past the resume screening process. After
three challenging technical interviews followed by an excruciatingly long host matching phase, I was ecstatic after
Google confirmed my position.

### Arsala Bangash - SETI Intern at Google Cloud Sunnyvale

![](https://lh3.googleusercontent.com/meG03nIC55JzoOTPrinSltAbwhFkApF5VZJGQT1pnxwYVF-OZCAOV-ekVlBYH1d24zhtoVumj7QgHzbZnHPV1TVeAwz4I8q4zsG_sYHLZVqPrDljwtsn1-XD-QH1SbpjVcG8FFNo=s0)

During orientation, I was awe-struck. Sometimes I couldn't believe it was real. I was fascinated by the infrastructure
and atmosphere around me, and I aimed to make the most of this experience.

I genuinely appreciated how much effort Google put into their interns' professional development, but I started to get an
uneasy feeling as the weeks went by.

Many experiences formed this feeling, but three, in particular, allowed the truth to really dawn upon me:

- The legal workshop during my intern orientation week
- [The Google-Pentagon AI Scandal](https://www.bbc.com/news/business-44341490)
- [The Censored Google for China Scandal](<https://en.wikipedia.org/wiki/Dragonfly_(search_engine)>)

Google was not fundamentally oriented towards helping humanity in the way that its marketing campaigns claimed.

Google was a corporation mandated to maximize its shareholder value and cater to the interests of powerful global
forces.

Still, life went on, and I had a great summer where I managed to grow professionally, form memories with other Googlers,
and secure an internship at Vida Health for the coming semester.

### Things were going fine…until they weren't.

#### It was around November 2018 when the reality of death truly dawned on me.

I found blood after I went to the bathroom one day, and I was thrust into the world of managing chronic illness.

![https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.artstation.com%2Fartwork%2Fk4g0ll&psig=AOvVaw2_l6f-FaolTzIUFVidemXG&ust=1624707974267000&source=images&cd=vfe&ved=0CAoQjRxqFwoTCOCFjYDbsvECFQAAAAAdAAAAABAP](https://lh4.googleusercontent.com/KNdsVBIYBogBp_RKirzb98qJZ2hmYmZHFkK5_yVCniLUOQmSJo9jAUnJMCDvDpHazEKXsYau2mQYiV-jo_dgKo3oB3VuQne39TcgSzgFpl4tD8UJaqEOtsyLElxLeeGXRV_gd4Lc=s0)

During times of isolation and uncertainty, I reflected on my life and the path I was walking.

I felt something deeply wrong… like I had forgotten what was important in life.

- To love and nurture your family.
- To work towards more peace and prosperity for the future generations of life.
- To create technology for sustainable human ecology.
- To educate the future generations about the world they were going to inherit.

I was in a system that was molding me to serve its purposes.

I felt like there was a lack of a greater perspective on how our human societies were conducting themselves.

Was I unconsciously bending myself to fit in a soulless system?

![](https://lh3.googleusercontent.com/IIOM0JbOvStfAHlICSzrumVfzyQcp04mMnb8HllbJrGJIgJc4lQP9J66a4tFDrOJuUV_XXJ1dk1c4mJywrO7gmoHu5WAgZ8ShUYqMz2yTPO-9BqAXBRZXmRDIi105H3c3CDq0Hgk=s0)

No more!

I vowed to orient myself towards serving humanity.

To the degree that I was able, I would stand up to the systems that live by the suffering of people everywhere.

At the same time, my mind was consumed by the idea that life was full of polarities.

Progressivism and conservatism.

Introversion and extroversion.

Idealism and pragmatism.

Black and White.

Without managing these polarities, we risked creating an increasingly divided world with less understanding and more
suffering.

To be Grey is to seek balance and harmony between the polarities in life.

Our blessing as conscious reasoning beings was our ability to be Grey.

![](https://lh3.googleusercontent.com/wecZr3Qp-UUdOrLCPDUHwc0tJw2CJGkAk8E8NrMXJXAWm5EIMmgqrxENCEoMevdp098LpBLx2Oads_2AnHgI2qf2I1_BhntKt2HHqzmoyNTCzrsT2CjNJLHj3jH-DcSGhFgIBnvv=s0)

Were the institutions I was serving approaching their polarities in a healthy way?

Were tech companies oriented towards providing meaningful value to the people they serve or making money for their
owners and shareholders?

Suppose that the companies creating software products and services for us are soulless, profit-maximizing machines at
their legal core. In that case, it's no surprise that they exploited and manipulated the very people they were created
to serve.

![](https://lh4.googleusercontent.com/--enIh-eO8OnGg87ad5RI37khMlgGCgA5GhN7eM0CIcFH31D9-hlEhf_Jmvl-tcGdINfFMu97o6X7qLuzTIv1QZrmB4eJn4V17-AUaURWvX33N2HvdbztJUYNFmo8wBMVpIWOfm3=s0)

You can read more about the Grey Software philosophy [here](https://grey.software/philosophy).

At the time, I had no definitive plans for starting a software company oriented towards the Grey. I had to be patient
and allow this idea to grow gradually.

---

During the summer of 2019, I returned to Google to intern on the open-source Android Studio project.

I was disillusioned with the narrative Google had painted in my previous internship, but I still had hope.

The Android OS provided compact computing power at a great price point to the masses, and it was an honor for me to work
on the same floor as some of the Android people I had watched on Google's IO conference Youtube videos.

I felt that life had set the stage for me to have an impactful summer at the Bellevue office.

![](https://lh4.googleusercontent.com/fAc9o49vZ7bSK8UKRXRGG1mRBbPIe7CWhLD1wRJa9hbw3QjdNTknWcHu8iQoYNERiuG9jZMPhTz7LN7SOp2LB0SrKpCEUpDtWtqn1Q0RdJMpw79P9w-rN9zRN7Cxs_Y6w0itDfm8=s0)

I came in with lots of energy and enthusiasm, working passionately on my project and my health. I had accepted that
there might be scandals and frustrations this time around, but I was determined.

Near the end of the internship, I still had faith despite witnessing:

- Mandatory employee training exercises that were out-of-touch with reality
- [More foul play by Google's products such as Youtube](https://www.youtube.com/watch?v=fDqBeXJ8Zx8)
- [The Google “Machine Learning Fairness” Whistleblower](https://www.projectveritas.com/news/google-machine-learning-fairness-whistleblower-goes-public-says-burden-lifted-off-of-my-soul/)

I had faith because, amid this chaos, I believed that Google was still capable of being a force for good in this world.
I knew that the people I worked with were kind-hearted and dedicated, so I wanted to leave them with an inspiring
message to remind them of their positive impact on people worldwide.

This was the message that I hoped to convey during my final presentation, and it was the message that I couldn't get to
because our engineering team manager stopped my presentation before I had the chance to finish.

I wanted to paint a truthful picture of my internship, which is why I spoke about how Google's scandals affected me in
addition to showcasing my project.

![](https://lh3.googleusercontent.com/hywBXncGLKIWH62mdBdwq8Rk7yHFFd9kMkR8lSz5UbWxzmsxhk7F6RUlG-3eFDJBFS6NGwwTY5ELqWcMcWQ94zJ0y9naDfszNvkgiSCB98ZZfUvJsqMUZtW9BlKiDC_eVdAIhV1L=s0)

When I showed a slide containing the image above from
[a video exposing the Youtube trending algorithm's unfairness](https://www.youtube.com/watch?v=fDqBeXJ8Zx8), the
engineering manager stopped my presentation, claiming I violated Google's code of conduct.

I remember standing with trembling knees and a tight throat as I asked for confirmation.

"Am I violating the code of conduct?"

"Yes."

I left the presentation room anxious about the future consequences of what I had just done, but my internship host
helped calm me down and navigated the Google code of conduct document with me. We found one ambiguously written clause
that encouraged Googlers to leave contentious political matters outside of the workplace.

When I left the office that day, I remember being heartbroken at the state of an institution I had genuinely fallen in
love with. I was grateful to have been able to confide in my brother and some fellow interns about how I was feeling,
and I was fortunate to have a meaningful conversation the following day.

On my last day at Google, I had a one-on-one meeting with my team manager (not the same manager who stopped my
presentation), who candidly explained the realities of the situation to me.

Paraphrased from my manager:

"Arsala, I have a family to raise in Seattle. Living here and ensuring that my kids get the best life is expensive, and
I work here because it's one of the best jobs available. For Google to do what it does at the scale it exists, they need
to foster a neutral environment where people with differing world views can work together. You're young, and you have
energy…maybe you would find it more meaningful to work at a not-for-profit."

![](https://lh3.googleusercontent.com/HDdTX0F4a3iNg4c_P3lbrkjkWDZQzn-hd8HVTilxajKFmsU8wlPe1_QnUhYNjj8QXKedIBAWZJZb7N9YIcI35xGnDheAQVLRPkw_sk1AgyD74vNEyCIUbJIX8JDStDeB-n7YmIk9=s0)

---

That fall, after my Google internship, I returned to Mississauga to complete my education at UTM.

I had learned a lot during the 16-month internship break I had taken from school, and I was grateful that several
students in the senior class were as passionate as I was about helping the incoming students walk more prosperous paths.

We did our best that year and came away with a massive victory for the student community.

Our university department agreed to form the CSSC (Computer science student society), staffed with three paid part-time
students and a supervisor from the department. This initiative was mandated to serve, support, and guide CS students.

![](https://lh3.googleusercontent.com/HAAq9TwArSMzIMCWiNS2fLGrQRG1DLsRh3Ir6MbGo3TdJG87t7Ce07Vhf5-tefs3qAKGfXVAqFbABSef5SHo_1-yv3CckhT-zwrm012KcUEfahBjhbL6kFsYnSZQgnxxiudQfK1a=s0)

During the academic year, I tried helping as many students as I could to bring their software ideas to life through
mentorship and resources. As a byproduct, some of Grey Software's foundations were formed. By this time, I was openly
using the name and encouraging students to go beyond their department-constructed curriculums and explore the world of
open-source software development.

As the school year ended strangely after the world was turned upside down by COVID19, I pondered whether I could start
building the Grey Software organization.

I didn't have a huge funding source or the entire skillset needed to pull this venture, but I knew the internet had the
answers I was looking for.

I had people tell me I was making a financially irresponsible move by not working for the kinds of companies that would
hire me, but I knew the tech industry would still be around if I decided to jump back in.

I didn't even have a clear business plan.

![](https://i.imgur.com/uSiTs3M.png)

What I had was an exhilarating albeit blurry vision of a world where anyone learning software could collaborate with
professionals to create useful products and services with the latest tech.

I knew how fortunate I was to have had the opportunity to study at UTM and intern at Google and Vida. I also knew how
happy it would make me to see every honest, determined student around the world have a fair shot at creating a better
future for themselves through technology.

Never in the history of the world did we have an event like COVID spike an exponential rise in distributed, remote
solutions.

My gut told me it was the right time to start building Grey Software full-time.

Armed with an exciting vision and the determination to make it a reality, I joined the Pioneer global startup
tournament.

![](https://lh6.googleusercontent.com/oKO_3JXw9SYEKvGg2xW-TQIrHdMRIrPjzqxJ-ii4mfeSF5hcCRO2eIfNuS1jOAHp52c7VCcRih2jL0cVnrXx92uIek9opubHrL3PSNdrIKt6yHObiqwysYSuw0kwVZ1jO9NsfTJx=s0)

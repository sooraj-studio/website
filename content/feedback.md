---
title: Feedback
description: Read more about what people are saying about Grey Software!
---

## Feedback From Students

Even though tons of youtube videos are available, it’s a different experience to have someone that understands what you
are going through to guide you in the project. - **Shawn, Student at UTM**

I have been programming on the side for the past year now, mainly using online resources to pair program... yet one
session with my mentor was more enriching than hours spent on these side tutorials. - **Osama, Grey Software Explorer**

I got to experience what it was like being onboarded into a codebase and collaborating with a skilled developer and
designer. I got the software development education I was looking for." - **Milind, Grey Software Apprentice**

### Raj Paul - Design Contributor To Team Member

As I finished my Design Bootcamp in August 2020, I was looking for some hands-on experience before I actively applied to
jobs. And being a part of Grey Software exactly provided and continues to provide that experience. As one of the early
contributors, my hands were in every aspect of Grey Software starting from copywriting to UI/UX.

Working with Grey Software has helped me learn new workflows for work and time management both at the professional and
personal levels.

As I complete one year at Grey Software as Digital Designer and contributor, I further plan to strengthen the brand in
every aspect and give it a stronger foundation on which it can stand upon to help more students in the future.

Open Source Contribution is an open playground to master your passion and be valuable at every step of the process!

### Avi - Apprentice To Contracted Engineer and Founding Member of Focused Browsing

It was a challenge for me to find my place as a software creator in the tech space after finishing my Bachelor's degree.
I recognized that although I had one internship experience, I knew there was still space for me to improve my
development skills, especially from a creative standpoint. I wanted to be able to build something on my own that people
can use for the betterment of their lives. All it took was witnessing what was happening in the world and a few
conversations with Arsala... and we knew we wanted to build Focused Browsing.

Being a part of the Focused Browsing team at Grey Software enabled me to become a far better web developer while also
pushing me to take initiative to build software from the bottom up. Some moments required a great deal of persistence,
from experimenting with different technologies to optimizing the extension. However, seeing Focused Browsing getting
released to the public has brought me true joy and satisfaction. To see myself use this extension along with other
people, made me recognize the power of open-source software. As long as we have the right tools and dare to try
something new, anything is possible!

### Osama - Open Source Apprentice

_Osama was an apprentice with Grey software during the summer of 2020, Where he contributed to a plethora of projects
passionately learned tools and technical knowledge related to modern web-apps and devops_

Reflecting on my work a few months later, I have had a massive technical boost being part of Grey Software throughout
the summer. Besides receiving valuable mentorship and working on meaningful projects, I was also exposed to a highly
professional workflow backed by Arsala's expertise and dedication to the organization.

I can confidently say that my time at Grey Software has enabled me to become more thorough in the technical work I do
and motivated to tackle more complex technical challenges with confidence in my abilities. As a result of the experience
I've gained, I have managed to go from a summer with only a single interview during my 2nd year to interviewing at some
great companies during my 3rd year, including Amazon, BlackBerry, ProofPoint, and Telus.

Open Source work is a truly enriching prospect, going beyond the grind of academic work. In addition to the technical
skills it provides, you also gain valuable insight into some of the great ideas people out there have had and get to
contribute to them. At Grey Software, I was delighted by the approach to immerse, educate, and reflect upon best
practices and my professional development :)

### Faraz - University Apprentice To Contracted Engineer

It has been a truly invaluable learning experience for me. There were many takeaways I gained through my experience at
Grey Software. I gained a new sense of professionalism, learned the importance of version control in open source
development and the value of writing high-quality code with collaborative reviews and pair programming.

I started my apprenticeship at Grey Software as a newbie developer as I had little to no experience working on any real
projects with a team. But after completing my apprenticeship and working with the company as a professional, I have
gained a new level of confidence to solve problems with code.

## Feedback on our Open Source Apprenticeship Program

### Structure & Mentorship

Having a leader that has built several successful projects allows for a cleaner road-map, thus preventing premature
production. - Shawn, Student at UTM

It was nice to have a mentor and professor to bounce ideas off of, and build within a structure. Building in a way
that's scalable in a team is hard, and I'm glad to have given it a try. - Arnav, Student at UTM

Arsala did a great job as a mentor since he understands the challenge that a 2nd year student could be facing. He also
understands the situation that most of his students have no experience with software development as a team before. His
constant advice and critiques helps the team to steadily move toward the right direction and for students to grow. -
Brian, Student at UTM

### Industry Workflow

This course simulated what it will be like to work in the industry by encouraging the students to work asynchronously
and proactively rather than following the instructions of an assignment. - **Shawn, Student at UTM**

I had to get used to a different mindset; working asynchronously and making progress daily instead of cramming before a
deadline. - **Lee, Student at UTM**

The most useful was the overall workflow because it can be applied to any future project. All projects need proper
documentation and remote repo, and group members need to work asynchronously by managing branches, pull requests, issues
and milestones. **- Baichen, Student at UTM**

## Feedback From the Open Source World

If you want to learn to code, go check out Grey Software! They help you contribute to OSS and learn while doing it. -
**Matic Zavadlal, Creator of SwiftGraphQL**

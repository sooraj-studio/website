---
title: Pitch
description: Check out Grey Software's startup pitch!
---

> Audience: Angel Investors and Venture Capital Firms
>
> Goal: Raise $50k+ non-equity-dilutive capital to help us continue our mission!

## Intro

![Slide 0](/pitch/0.png)

## The Problems With Modern Software Education

![Slide 1](/pitch/1.png)

## The Key Insight

![Slide 2](/pitch/2.png)

  <details>
    <summary>Expand To Learn More About Our Vision For Authentic, Up-To-Date, and Scalable Software Education</summary>
    
## Authentic...But How?

![Slide 5](/pitch/5.png)

## Up-To-Date...But How?

![Slide 6](/pitch/6.png)

## Scalable...But How?

![Slide 7](/pitch/7.png)

</details>

## Introducing Grey Software

![Slide 3](/pitch/3.png)

<details>
    <summary>Expand To Learn More About What We Do</summary>
    
## Open Software

![Slide 20](/pitch/20.png)

## Open Education

![Slide 21](/pitch/21.png)

## Open Collaboration

![Slide 22](/pitch/22.png)

</details>

## How Your Investment Can Help Us

![Slide 4](/pitch/4.png)

## Our Business Model

![Slide 23](/pitch/23.png)

<details>
    <summary>Expand To Learn More About Our Business Model & Strategy</summary>

## Strategy: Open Software

![Slide 25](/pitch/25.png)

## Strategy: Open Education

![Slide 26](/pitch/26.png)

## Strategy: Open Collaboration

![Slide 27](/pitch/27.png)

## Strategic Map

![Slide 28](/pitch/28.png)

</details>

## Our Impact: 16 Months

![Slide 29](/pitch/29.png)

## Impact: Open Software

![Slide 30](/pitch/30.png)

## Impact: Open Education

![Slide 31](/pitch/31.png)

## Impact: Open Collaboration

![Slide 32](/pitch/32.png)

## The Competition

![Slide 18](/pitch/18.png)

<details>
    <summary>Expand To Learn More About Our Competition</summary>

- Mozilla creates high quality open source products to promote internet freedom, and manages the Mozilla Developer
  Network which is one of the world’s best sources for web development education.

- FreeCodeCamp is a not-for-profit company that creates videos, articles, and interactive coding lessons - all freely
  available to the public.
- Khan Academy is a nonprofit with the mission of providing a free, world-class education for anyone, anywhere. They
  have Courses about computer science but don’t delve as deep as Free Code Camp with respect to industry software
  engineer and data science.
- MLH has an excellent partnership network with universities and tech companies around the world. With this, they run
  events, competitions, and open source apprenticeship programs for students get practical software development
  experience.

</details>

## Our Future Objectives

![Slide 33](/pitch/33.png)

## Our Founder

![Slide 36](/pitch/36.png)

## Your Investment Options

![Slide 35](/pitch/35.png)

## Thank You

We appreciate you taking the time to go through our pitch 😃

If you would like to proceed, please reach out to us at our org@ email!

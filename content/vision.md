## Mission

We empower people to create and use open-source software for more liberated and peaceful lives.

## Vision updated

A world of empowered professionals collaboratively creating open-source software that we trust, love, and learn from.

## Values

### Creative Empowerment

We are committed to empowering today's learners to create open-source software that will shape the future of their
communities and societies. We do this by equipping our students and contributors with the latest software development
tools and processes.

### Products and Education

We are committed to delivering a high-quality software education curriculum alongside the products we develop. We strive
to create unique, world-class software products while engaging communities in our development process.

### Academic and Industrial Collaboration

We are committed to catalyzing collaboration between academia and industry to bridge the gap between what students learn
and what professionals use. We do this by running for-credit programs for students with partnered universities.

### Higher Purpose

We are committed to developing software that enriches peoples' lives, not shareholders' bank accounts. When we create,
we orient ourselves towards values of courage, love, joy, hope, and peace.

## Strategy

### Open Products

We develop our products with permissive open source licenses and encourage contributors from all over the world to help
shape the future of our creations. We aim to sustain ourselves by developing revenue-generating applications that you
can contribute to.

### Open Education

We develop open educational resources and partner with universities for cooperative education programs. Through this, we
facilitate an agile, open-source software development environment where students gain practical experience.

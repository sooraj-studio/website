---
title: Hacktoberfest 2021
description: Grey software is participating in  Hacktoberfest 2021! Learn more about how you can contribute!
---

![Preview](https://hacktoberfest.digitalocean.com/_nuxt/img/logo-hacktoberfest-full.f42e3b1.svg)

# Get Started

We recommend that you get started with completing our onboarding exercise and signing yourself up as a Grey Software
explorer.

You **will get hacktoberfest credit** for onboarding successfully!

<cta-button text="Start Onboarding" link="https://onboarding.grey.software"></cta-button>

# Projects

## Resources Website

With your help, we can build a library of useful online resources for people worldwide to benefit from!

<cta-button text="Contribute" link="https://resources.grey.software/contributing"></cta-button>
